## Final Project - TICK42

This document describes the final project assignment for Telerik Academy.

# Online Whiteboard Platform - Team 10 

## General Project Requirements

## Project Description:

It is 2020 and COVID-19 has taken the world by storm. The use of collaboration and conferencing software is at all time high. You see this as a great business opportunity. Your plan is to develop an online whiteboard platform that would allow teams to collaborate while working from home. Fortunately for you during this difficult time you acquired some important programming skills that will help you along the unicorn startup way.  

Your task is to develop an online whiteboard platform for visual collaboration using ReactJS. You can think of a board as an infinite canvas that can capture the wildest of ideas. Users need to be able to register and login, create empty boards, manage existing boards and view a list of all boards. When inside of a board they need to be able to do one of the following:

⦁	draw  
⦁   mind map  
⦁	create  
⦁	Kanban boards  
⦁   design databases  

Investors are eagerly waiting for your demo in 4 weeks! Good luck!
Here are examples of similar collaboration platforms: Miro, AWW App  

##### The application MUST have:  
⦁	public part  
⦁	private part  
⦁	administration part  

## Public Part:
##### The public part of the project MUST be visible without authentication.  
⦁	Application MUST have a landing page  
⦁	Briefly explain the product and its features  
Examples: Lemonade, Stripe  

⦁	User MUST be able to register/login    
⦁	The authentication SHOULD be token based    
⦁	User SHOULD be able to recover password    

## Private Part:
##### The private part of the system MUST be accessible after a successful authentication.  
⦁	Application MUST have a public boards page  

##### All boards are public by default (private boards are a SHOULD requirement described in the Administration Part).  
⦁	Clicking on a board MUST open the board in its latest state   

## Administration Part:
##### Every user is an administrator.  

⦁	Application MUST have a boards management page  
⦁	User MUST be able to create/delete boards  
⦁	User SHOULD be able to rename boards  
⦁	User SHOULD be able to change password  
⦁	Application SHOULD have a private boards page

##### Private boards MUST be accessible only by the user that created them.  

⦁	User MUST be able to toggle the board visibility between public and private inside the boards management page.  
⦁	User MUST be displayed an error page when trying to access a private board of another user.

## Boards:
##### These requirements apply to both public and private boards (private boards are a SHOULD requirement described in the Administration Part).

⦁	Board state MUST be preserved.  
⦁	After each change (e.g. adding a Kanban card) the board state MUST be updated and saved.  
⦁	Opening a board MUST display its latest state. 

## Choose a core functionality (the other become COULD requirements):

### User MUST be able to draw
#### Features:  

⦁	User MUST be able to draw using a pencil and a brush   
⦁	User MUST be able to change the color of the pencil/brush   
⦁	User MUST be able to erase  
⦁	User MUST be able to create text boxes  
⦁	User SHOULD be able to format the text of the text boxes  
⦁	User SHOULD be able to change the thickness of the pencil/brush  
	              Example: https://jspaint.app/   

### User MUST be able to mind map    
#### Features:   

⦁	User MUST be able to create root, child and sibling nodes  
⦁	User MUST be able to connect the nodes using edges  
⦁	User MUST be able to label the nodes/edges  
⦁	User SHOULD be able to drag and drop nodes  
⦁	User SHOULD be able to assign colors to nodes  
⦁	User SHOULD be able to format the text of the labels  
⦁	User SHOULD be able to change the thickness of the edges    
	              Example: https://app.mindmup.com/map/new/    

### User MUST be able to create Kanban boards    
#### Features:  

⦁	User MUST be able to add/delete columns  
⦁	User MUST be able to add/delete cards  
⦁	User MUST be able to rename columns/cards  
⦁	User MUST be able to drag and drop cards between columns  
⦁	User SHOULD be able to assign colors to columns/cards  
⦁	User SHOULD be able to assign cards to users  
⦁	User SHOULD be able to assign planned start and due dates to cards  
⦁	User SHOULD be able to export the Kanban board as a 'Gantt chart'    
	              Example: https://kanbanize.com/wp-content/uploads/website-images/kanban-resources/product_portfolio_board.png    

### User MUST be able to design databases    
#### Features: 

⦁	User MUST be able to add/delete tables    
⦁	User MUST be able to add/delete fields    
⦁	User MUST be able to assign relationships (1 to 1, 1 to many and many to many) between tables    
⦁	User MUST be able to put constraints on fields    
⦁	User SHOULD be able to assign colors to tables/fields    
⦁	User COULD be able to auto arrange the tables    
	              Example: https://dbdiagram.io/d    
                  
### Other:  
##### Other requirements not exclusively dedicated to boards.  
⦁	User SHOULD be able to toggle between light and dark color theme.     
⦁	User SHOULD be able to collaborate with other team members.    
Hint: Use ⦁	WebSockets for the client-server communication.    
⦁	User MUST be able to invite other people to the board (e.g. via shareable links)  
⦁	User SHOULD be able to see the other team members' mouse pointers  
⦁	User SHOULD be able to chat with other team members  
⦁	User SHOULD be able to add comments to items    
⦁	User SHOULD be able to zoom in and out    
⦁	User MUST be displayed the current zoom level  
⦁	User SHOULD be able to receive notifications (e.g. when mentioned inside a comment by another team member)    
⦁	User COULD be able to export a board as a PDF    
⦁	User COULD be able to backup and restore backups of boards    
⦁	User COULD be able to use keyboard shortcuts for different functionalities (e.g. switch from pencil to brush and vice versa)    
⦁	Application COULD support ⦁	i18n    
⦁	User COULD be able to review activity history    
⦁	User COULD be able to undo and redo actions     

## Development Requirements  
##### Requirements related to the technologies, frameworks and development techniques used by the web application.  
⦁	MUST:   
⦁	Use ReactJS    
⦁	Create a beautiful and responsive UI    
⦁	Use Git for source control  
⦁	Use GitHub   

## Take advantage of branches for the development of the different features  
⦁	Pass the default ESLint react-app linting configuration without any errors  
⦁	SHOULD:  
⦁	Host the web application
⦁	Use TypeScript

## Use all strict type checking options  
⦁	Write unit tests for 3 non-trivial components  
⦁	Write integration tests  
⦁	Add ⦁	Glue42 Core integration (e.g. split the application into micro front-ends and use the windows/app manager APIs to create a more seamless UX; replace a modal with a Glue42 Core window)    
⦁	COULD:  
⦁	Document the project, project structure and project architecture    

## Include screenshots

## Trainer Project Defenses:  
##### Each student must make a trainer defense of its work to the trainers (~45-60 minutes). It includes:

⦁	Live demonstration of the developed web application (please prepare sample data)  
⦁	Explain application structure, source code and design decisions  
⦁	Show the commit logs in the source control repository to prove a contribution from all team members  

We greatly value teams who can do their job cleanly with a logical and maintainable design, without either unnecessary abstraction or ad hoc hacks.
You need to understand the system you have created. Any defects or incomplete functionality must be properly documented and secured. It is OK if the proof of concept of your application has flaws or is missing one or two MUSTs. What is not OK is if you do not know what is working and what is not and if you present an incomplete project as functional.  

##### Some things you need to be able to explain during your project presentation:  

⦁	What are the most important things you have learned while working on this project?  
⦁	What are the worst “hacks” in the project, or where do you more work is required?  
⦁	What would you do differently if you were implementing the system again?  

## Partner Project Presentation:  
##### Each team will have ~10 minutes to present their project in front of the trainer, partner representatives and fellow students.
During the presentation, you can:  

⦁	Do a live demonstration of the developed web application (please prepare sample data)  
⦁	Explain application structure, source code and design decisions  

## Partner Project Defense:  
##### Each team must make a partner defense of their work (~30 minutes).  
Partner representatives will evaluate the project according to the following criteria: 

⦁	Feature Completeness and Quality  
⦁	Have all requirements been met? Have optional requirements been implemented?  
⦁	Are there any issues with performance, bugs, or malfunction?   
⦁	Code Architecture and Design Quality  
⦁	Loose coupling  
⦁	Modularity    
⦁	Extensibility  
⦁	User Experience and Creativity  
