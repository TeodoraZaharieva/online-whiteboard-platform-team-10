```🌈 Welcome to WeeGEN Whiteboard Platform!!!```
 <br>

This overview describes what is for the WeeGEN's API, how to prepare and use it. 🎨 
The API is completely free and its an open source code suggesting improvements. 🐾 
 <br>
 <br>

```Basic Features```

WeeGEN's API is a is simple to use web-based application which you can use to draw your amazing ideas every day. The interface is very intuitive and you could easily see the basic options that could be used in the application. The WeeGEN's API gives you the freedom to draw on a white board with an always ready to generate new ideas pencil that uses different colors. Each canvas could be saved in a personal collection that can be reviewed or edited.  
<br>
There are several options available up there on the board once you have logged successfully into the WeeGEN's API: 
<br> 

```NEW CANVAS``` - opens a clean new whiteboard that contains the basic tools: 
<br>

    BRUSH:
    With the Brush you can draw a freehand line. Each single line is draggable which allows you to make a composition with your own figures.
<br>

    COLOR PALETTE: 
    If you see your idea with different colors, OK!  You have a nice 36 colors palette now from which to choose.  
<br>

    ERASER: 
    With the eraser tool you can make corrections of your drawing if needed. 
<br>

    SAVE: 
    The Save option saves your canvas. All your canvases are gathered in your private collection which is accessible whenever you need with going to MY CANVASES.  
<br>

```ACTIVE CANVAS``` - it shows you the last opened canvas on which you can draw. 
<br>

```MY CANVASES``` - collects your ideas that you have created. With your new inspiration, you can always go to MY CANVASES and dive again into a single canvas and make it grow! Each open canvas shows up the basic tools.  
<br>

    INSTALLATION
    To get started: 

- $ git clone https://gitlab.com/TeodoraZaharieva/online-whiteboard-platform-team-10.git 

- npm i  - on both client/server side 

- npm run start - on both sides 

- Open your browser and navigate to http://localhost:4000/ 
<br>

Once you open the application it will lead you to our Landing page where you could read some additional information for this project. 
<br> 

In order to access the drawing functionalities, you will need to Register first and then to Login. The options are available in the right up corner of the API and will ask you for credentials to provide. Once you have successfully logged in you are free to express yourself and most of all - have fun! 💚
<br> 

Want to logout? The Logout option it's up there next to your Display name. But don’t forget to go back and check your ideas from time to time - may be your best idea is waiting there for you to give it wo the world! 💎👀 
<br> 

Our Team appreciates that you have chosen the WeeGEN API and we believe that you will enjoy using it. 
<br>
And just to mention at last... it all started from a scratch! ✨ 
<br> 
<br>

```WeeGEN Team:``` <br>
💗💙💚💛💜 

Teodora Zaharieva <br>
Lyubka Marinova  
<br>

**********************************************************
The Project is written in: ```JavaScript, TypeScript, CSS, HTML5```. <br>
Technologies: ```NodeJS, NestJS, , MariaDB, ReactJS```. <br>
Libraries: ```TypeORM, KonvaJS, Bootstrap, Bootswatch```.   