import { AuthService } from "./auth.service";
import { UserLoginDTO } from "../dtos/login-user.dto";
import { UserCreateDTO } from "../dtos/user-create.dto";
import { UserDTO } from "../dtos/user.dto";
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    registerUser(user: UserCreateDTO): Promise<UserDTO>;
    login(user: UserLoginDTO): Promise<{
        token: string;
    }>;
    logout(): Promise<{
        message: string;
    }>;
}
