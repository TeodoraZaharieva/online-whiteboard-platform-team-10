import { JwtService } from "@nestjs/jwt";
import { Repository } from "typeorm";
import { User } from "../entities/user.entity";
import { UserLoginDTO } from "../dtos/login-user.dto";
import { UserCreateDTO } from "../dtos/user-create.dto";
import { UserDTO } from "../dtos/user.dto";
import { UserService } from "../services/user.service";
export declare class AuthService {
    private readonly userRepository;
    private readonly jwtService;
    private readonly userService;
    constructor(userRepository: Repository<User>, jwtService: JwtService, userService: UserService);
    register(user: UserCreateDTO): Promise<UserDTO>;
    findUserByName(username: string): Promise<User>;
    validateUser(username: string, password: string): Promise<User>;
    login(loginUser: UserLoginDTO): Promise<{
        token: string;
    }>;
}
