"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const jwt_1 = require("@nestjs/jwt");
const bcrypt = require("bcrypt");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("../entities/user.entity");
const user_service_1 = require("../services/user.service");
let AuthService = class AuthService {
    constructor(userRepository, jwtService, userService) {
        this.userRepository = userRepository;
        this.jwtService = jwtService;
        this.userService = userService;
    }
    async register(user) {
        return this.userService.registerUser(user);
    }
    async findUserByName(username) {
        return await this.userRepository.findOne({
            username,
            isDeleted: false,
        });
    }
    async validateUser(username, password) {
        const user = await this.findUserByName(username);
        if (!user) {
            return null;
        }
        const isUserValidated = await bcrypt.compare(password, user.password);
        return isUserValidated ? user : null;
    }
    async login(loginUser) {
        const user = await this.validateUser(loginUser.username, loginUser.password);
        if (!user) {
            throw new common_1.UnauthorizedException("Wrong credentials!");
        }
        const payload = {
            id: user.id,
            username: user.username,
            displayName: user.displayName,
        };
        const token = await this.jwtService.signAsync(payload);
        return {
            token,
        };
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.User)),
    __metadata("design:paramtypes", [typeof (_a = typeof typeorm_2.Repository !== "undefined" && typeorm_2.Repository) === "function" ? _a : Object, typeof (_b = typeof jwt_1.JwtService !== "undefined" && jwt_1.JwtService) === "function" ? _b : Object, user_service_1.UserService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map