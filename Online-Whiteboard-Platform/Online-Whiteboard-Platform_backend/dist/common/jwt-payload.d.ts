export declare class JwtPayload {
    id: number;
    username: string;
    displayName: string;
}
