import { CanvasCreateDTO } from "../dtos/canvas/canvas.create.dto";
import { CanvasDTO } from "../dtos/canvas/canvas.dto";
import { CanvasService } from "../services/canvas.service";
export declare class CanvasController {
    private readonly canvasService;
    constructor(canvasService: CanvasService);
    createCanvas(canvasCreateDto: CanvasCreateDTO, userId: any): Promise<Partial<CanvasDTO>>;
    getUserCanvases(userId: string): Promise<Partial<CanvasDTO>[]>;
}
