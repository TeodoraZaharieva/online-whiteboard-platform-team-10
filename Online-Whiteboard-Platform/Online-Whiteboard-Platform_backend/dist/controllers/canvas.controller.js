"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CanvasController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const canvas_create_dto_1 = require("../dtos/canvas/canvas.create.dto");
const canvas_service_1 = require("../services/canvas.service");
let CanvasController = class CanvasController {
    constructor(canvasService) {
        this.canvasService = canvasService;
    }
    async createCanvas(canvasCreateDto, userId) {
        return await this.canvasService.createCanvas(canvasCreateDto, userId.userId);
    }
    async getUserCanvases(userId) {
        return await this.canvasService.getUserCanvases(+userId);
    }
};
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard("jwt")),
    __param(0, common_1.Body()),
    __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [canvas_create_dto_1.CanvasCreateDTO, Object]),
    __metadata("design:returntype", Promise)
], CanvasController.prototype, "createCanvas", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Param("userId")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CanvasController.prototype, "getUserCanvases", null);
CanvasController = __decorate([
    common_1.Controller("api/:userId/canvases"),
    __metadata("design:paramtypes", [canvas_service_1.CanvasService])
], CanvasController);
exports.CanvasController = CanvasController;
//# sourceMappingURL=canvas.controller.js.map