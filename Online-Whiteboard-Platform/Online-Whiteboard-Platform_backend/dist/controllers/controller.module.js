"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ControllersModule = void 0;
const common_1 = require("@nestjs/common");
const service_module_1 = require("../services/service.module");
const auth_controller_1 = require("../auth/auth.controller");
const lines_controller_1 = require("./lines.controller");
const canvas_controller_1 = require("./canvas.controller");
let ControllersModule = class ControllersModule {
};
ControllersModule = __decorate([
    common_1.Module({
        imports: [service_module_1.ServicesModule],
        controllers: [auth_controller_1.AuthController, canvas_controller_1.CanvasController, lines_controller_1.LinesController],
    })
], ControllersModule);
exports.ControllersModule = ControllersModule;
//# sourceMappingURL=controller.module.js.map