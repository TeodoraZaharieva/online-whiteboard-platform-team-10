import { LineCreateDTO } from "../dtos/line/line.create.dto";
import { LineDTO } from "../dtos/line/line.dto";
import { LineService } from "../services/line.service";
export declare class LinesController {
    private readonly lineService;
    constructor(lineService: LineService);
    createLine(canvasId: number, lineCreateDto: LineCreateDTO, request: any): Promise<LineDTO>;
}
