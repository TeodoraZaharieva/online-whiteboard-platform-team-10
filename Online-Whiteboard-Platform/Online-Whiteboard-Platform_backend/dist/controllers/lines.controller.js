"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LinesController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const line_create_dto_1 = require("../dtos/line/line.create.dto");
const line_service_1 = require("../services/line.service");
let LinesController = class LinesController {
    constructor(lineService) {
        this.lineService = lineService;
    }
    async createLine(canvasId, lineCreateDto, request) {
        return await this.lineService.createLine(+canvasId, lineCreateDto, +request.user.id);
    }
};
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard("jwt")),
    __param(0, common_1.Param("canvasId")),
    __param(1, common_1.Body()),
    __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, line_create_dto_1.LineCreateDTO, Object]),
    __metadata("design:returntype", Promise)
], LinesController.prototype, "createLine", null);
LinesController = __decorate([
    common_1.Controller("api/canvases/:canvasId/lines"),
    __metadata("design:paramtypes", [line_service_1.LineService])
], LinesController);
exports.LinesController = LinesController;
//# sourceMappingURL=lines.controller.js.map