import { LineCreateDTO } from "../line/line.create.dto";
export declare class CanvasCreateDTO {
    name: string;
    isPublic: boolean;
    lines: LineCreateDTO[];
}
