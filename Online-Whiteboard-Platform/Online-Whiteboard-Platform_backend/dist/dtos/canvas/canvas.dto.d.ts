import { LineDTO } from "../line/line.dto";
import { UserDTO } from "../user.dto";
export declare class CanvasDTO {
    id: number;
    name: string;
    isPublic: boolean;
    author: UserDTO;
    lines: LineDTO[];
}
