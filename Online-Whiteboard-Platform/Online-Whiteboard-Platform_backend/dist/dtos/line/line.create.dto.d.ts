import { PointDTO } from "./point.dto";
export declare class LineCreateDTO {
    points: PointDTO[];
    stroke: string;
    strokeWidth: number;
}
