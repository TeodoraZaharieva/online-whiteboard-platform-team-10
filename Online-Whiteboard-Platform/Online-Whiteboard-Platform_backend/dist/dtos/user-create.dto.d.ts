export declare class UserCreateDTO {
    username: string;
    password: string;
    displayName: string;
}
