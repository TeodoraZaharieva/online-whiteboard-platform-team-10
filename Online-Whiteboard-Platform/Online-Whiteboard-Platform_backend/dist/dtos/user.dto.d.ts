export declare class UserDTO {
    id: number;
    username: string;
    displayName: string;
}
