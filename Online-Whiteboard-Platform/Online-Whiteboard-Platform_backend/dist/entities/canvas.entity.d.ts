import { Line } from "./line.entity";
import { User } from "./user.entity";
export declare class Canvas {
    id: number;
    isDeleted: boolean;
    isPublic: boolean;
    name: string;
    author: User;
    lines: Line[];
}
