import { Canvas } from "./canvas.entity";
import { Point } from "./point.entity";
export declare class Line {
    id: number;
    isDeleted: boolean;
    stroke: string;
    strokeWidth: number;
    canvas: Canvas;
    points: Point[];
}
