"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Line = void 0;
const typeorm_1 = require("typeorm");
const canvas_entity_1 = require("./canvas.entity");
const point_entity_1 = require("./point.entity");
let Line = class Line {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn("increment"),
    __metadata("design:type", Number)
], Line.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({
        type: "boolean",
        default: false,
    }),
    __metadata("design:type", Boolean)
], Line.prototype, "isDeleted", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Line.prototype, "stroke", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Line.prototype, "strokeWidth", void 0);
__decorate([
    typeorm_1.ManyToOne(() => canvas_entity_1.Canvas, (canvas) => canvas.lines),
    __metadata("design:type", canvas_entity_1.Canvas)
], Line.prototype, "canvas", void 0);
__decorate([
    typeorm_1.OneToMany(() => point_entity_1.Point, (point) => point.line, { eager: true }),
    __metadata("design:type", Array)
], Line.prototype, "points", void 0);
Line = __decorate([
    typeorm_1.Entity("lines")
], Line);
exports.Line = Line;
//# sourceMappingURL=line.entity.js.map