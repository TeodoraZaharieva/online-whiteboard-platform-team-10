import { Line } from "./line.entity";
export declare class Point {
    id: number;
    posX: number;
    posY: number;
    line: Line;
}
