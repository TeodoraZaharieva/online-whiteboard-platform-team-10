"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Point = void 0;
const typeorm_1 = require("typeorm");
const line_entity_1 = require("./line.entity");
let Point = class Point {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn("increment"),
    __metadata("design:type", Number)
], Point.prototype, "id", void 0);
__decorate([
    typeorm_1.Column("bigint"),
    __metadata("design:type", Number)
], Point.prototype, "posX", void 0);
__decorate([
    typeorm_1.Column("bigint"),
    __metadata("design:type", Number)
], Point.prototype, "posY", void 0);
__decorate([
    typeorm_1.ManyToOne((type) => line_entity_1.Line, (line) => line.points),
    __metadata("design:type", line_entity_1.Line)
], Point.prototype, "line", void 0);
Point = __decorate([
    typeorm_1.Entity("points")
], Point);
exports.Point = Point;
//# sourceMappingURL=point.entity.js.map