import { Canvas } from "./canvas.entity";
export declare class User {
    id: number;
    username: string;
    password: string;
    displayName: string;
    isDeleted: boolean;
    isBanned: boolean;
    createDate: Date;
    canvases: Canvas[];
}
