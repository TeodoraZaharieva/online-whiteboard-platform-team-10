import { Repository } from "typeorm";
import { CanvasCreateDTO } from "../dtos/canvas/canvas.create.dto";
import { CanvasDTO } from "../dtos/canvas/canvas.dto";
import { Canvas } from "../entities/canvas.entity";
import { User } from "../entities/user.entity";
import { Line } from "../entities/line.entity";
import { Point } from "../entities/point.entity";
import { TransformService } from "./transform.service";
export declare class CanvasService {
    private readonly pointRepository;
    private readonly lineRepository;
    private readonly canvasRepository;
    private readonly userRepository;
    private readonly transformService;
    constructor(pointRepository: Repository<Point>, lineRepository: Repository<Line>, canvasRepository: Repository<Canvas>, userRepository: Repository<User>, transformService: TransformService);
    getUserCanvases(userId: number): Promise<Partial<CanvasDTO>[]>;
    createCanvas({ name, lines }: CanvasCreateDTO, userId: number): Promise<Partial<CanvasDTO>>;
}
