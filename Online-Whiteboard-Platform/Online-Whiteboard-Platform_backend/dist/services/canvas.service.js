"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a, _b, _c, _d;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CanvasService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const canvas_entity_1 = require("../entities/canvas.entity");
const user_entity_1 = require("../entities/user.entity");
const line_entity_1 = require("../entities/line.entity");
const point_entity_1 = require("../entities/point.entity");
const transform_service_1 = require("./transform.service");
let CanvasService = class CanvasService {
    constructor(pointRepository, lineRepository, canvasRepository, userRepository, transformService) {
        this.pointRepository = pointRepository;
        this.lineRepository = lineRepository;
        this.canvasRepository = canvasRepository;
        this.userRepository = userRepository;
        this.transformService = transformService;
    }
    async getUserCanvases(userId) {
        const canvasesFound = await this.canvasRepository.find({
            where: { author: { id: userId }, isPublic: true, isDeleted: false },
        });
        return canvasesFound.map(this.transformService.toCanvasResponseDTO);
    }
    async createCanvas({ name, lines }, userId) {
        const userFound = await this.userRepository.findOne({
            where: { id: userId, isDeleted: false },
            relations: ["canvases"],
        });
        if (!userFound) {
            throw new common_1.NotFoundException(`User not found!`);
        }
        const createdLines = [];
        for (const line of lines) {
            const pointsToCreate = line.points.reduce((acc, pos, index) => {
                if (index % 2 === 0) {
                    acc.push({ posX: pos, posY: line.points[index + 1] });
                }
                return acc;
            }, []);
            const createdPoints = [];
            for (const point of pointsToCreate) {
                const pointToCreate = this.pointRepository.create();
                pointToCreate.line = null;
                pointToCreate.posX = point.posX;
                pointToCreate.posY = point.posY;
                const createdPoint = await this.pointRepository.save(pointToCreate);
                createdPoints.push(createdPoint);
            }
            const lineToCreate = this.lineRepository.create();
            lineToCreate.stroke = line.stroke;
            lineToCreate.strokeWidth = line.strokeWidth || 1;
            lineToCreate.points = createdPoints;
            const createdLine = await this.lineRepository.save(lineToCreate);
            createdLines.push(createdLine);
        }
        const canvasToCreate = this.canvasRepository.create();
        canvasToCreate.name = name;
        canvasToCreate.author = userFound;
        canvasToCreate.lines = createdLines;
        const createdCanvas = await this.canvasRepository.save(canvasToCreate);
        return this.transformService.toCanvasResponseDTO(createdCanvas);
    }
};
CanvasService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(point_entity_1.Point)),
    __param(1, typeorm_1.InjectRepository(line_entity_1.Line)),
    __param(2, typeorm_1.InjectRepository(canvas_entity_1.Canvas)),
    __param(3, typeorm_1.InjectRepository(user_entity_1.User)),
    __metadata("design:paramtypes", [typeof (_a = typeof typeorm_2.Repository !== "undefined" && typeorm_2.Repository) === "function" ? _a : Object, typeof (_b = typeof typeorm_2.Repository !== "undefined" && typeorm_2.Repository) === "function" ? _b : Object, typeof (_c = typeof typeorm_2.Repository !== "undefined" && typeorm_2.Repository) === "function" ? _c : Object, typeof (_d = typeof typeorm_2.Repository !== "undefined" && typeorm_2.Repository) === "function" ? _d : Object, transform_service_1.TransformService])
], CanvasService);
exports.CanvasService = CanvasService;
//# sourceMappingURL=canvas.service.js.map