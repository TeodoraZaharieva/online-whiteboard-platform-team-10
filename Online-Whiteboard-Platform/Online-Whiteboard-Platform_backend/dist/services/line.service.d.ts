import { LineCreateDTO } from "../dtos/line/line.create.dto";
import { LineDTO } from "../dtos/line/line.dto";
import { Canvas } from "../entities/canvas.entity";
import { Line } from "../entities/line.entity";
import { User } from "../entities/user.entity";
import { Repository } from "typeorm";
import { TransformService } from "./transform.service";
export declare class LineService {
    private readonly canvasRepository;
    private readonly userRepository;
    private readonly lineRepository;
    private readonly transformService;
    constructor(canvasRepository: Repository<Canvas>, userRepository: Repository<User>, lineRepository: Repository<Line>, transformService: TransformService);
    createLine(canvasId: number, lineCreateDto: LineCreateDTO, userId: number): Promise<LineDTO>;
}
