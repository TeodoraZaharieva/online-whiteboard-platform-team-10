"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServicesModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const passport_1 = require("@nestjs/passport");
const jwt_1 = require("@nestjs/jwt");
const jwt_strategy_1 = require("./strategy/jwt-strategy");
const secret_1 = require("../constant/secret");
const user_entity_1 = require("../entities/user.entity");
const canvas_entity_1 = require("../entities/canvas.entity");
const line_entity_1 = require("../entities/line.entity");
const point_entity_1 = require("../entities/point.entity");
const auth_service_1 = require("../auth/auth.service");
const user_service_1 = require("./user.service");
const canvas_service_1 = require("./canvas.service");
const line_service_1 = require("./line.service");
const transform_service_1 = require("./transform.service");
let ServicesModule = class ServicesModule {
};
ServicesModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([user_entity_1.User, canvas_entity_1.Canvas, line_entity_1.Line, point_entity_1.Point]),
            passport_1.PassportModule.register({ defaultStrategy: "jwt" }),
            jwt_1.JwtModule.registerAsync({
                useFactory: async () => ({
                    secret: secret_1.jwtConstants.secret,
                    signOptions: {
                        expiresIn: secret_1.jwtConstants.expiresIn,
                    },
                }),
            }),
        ],
        providers: [
            transform_service_1.TransformService,
            user_service_1.UserService,
            canvas_service_1.CanvasService,
            line_service_1.LineService,
            auth_service_1.AuthService,
            jwt_strategy_1.JwtStrategy,
        ],
        exports: [
            transform_service_1.TransformService,
            user_service_1.UserService,
            canvas_service_1.CanvasService,
            line_service_1.LineService,
            auth_service_1.AuthService,
            passport_1.PassportModule,
        ],
    })
], ServicesModule);
exports.ServicesModule = ServicesModule;
//# sourceMappingURL=service.module.js.map