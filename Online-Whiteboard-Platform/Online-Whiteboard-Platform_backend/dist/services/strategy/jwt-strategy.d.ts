import { JwtPayload } from "../../common/jwt-payload";
import { AuthService } from "../../auth/auth.service";
import { User } from "../../entities/user.entity";
declare const JwtStrategy_base: any;
export declare class JwtStrategy extends JwtStrategy_base {
    private readonly authService;
    constructor(authService: AuthService);
    validate(payload: JwtPayload): Promise<User>;
}
export {};
