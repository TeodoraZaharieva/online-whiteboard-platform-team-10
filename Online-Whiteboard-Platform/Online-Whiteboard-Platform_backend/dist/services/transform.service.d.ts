import { UserDTO } from "./../dtos/user.dto";
import { User } from "../entities/user.entity";
import { Canvas } from "../entities/canvas.entity";
import { CanvasDTO } from "../../src/dtos/canvas/canvas.dto";
export declare class TransformService {
    toUserResponseDTO(user: User): UserDTO;
    toCanvasResponseDTO({ id, name, isPublic, lines, }: Canvas): Partial<CanvasDTO>;
}
