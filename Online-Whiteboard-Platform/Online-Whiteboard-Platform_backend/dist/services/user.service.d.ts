import { Repository } from "typeorm";
import { UserDTO } from "../dtos/user.dto";
import { UserCreateDTO } from "../dtos/user-create.dto";
import { User } from "../entities/user.entity";
import { TransformService } from "./transform.service";
export declare class UserService {
    private readonly userRepository;
    private readonly transformer;
    constructor(userRepository: Repository<User>, transformer: TransformService);
    registerUser(user: UserCreateDTO): Promise<UserDTO>;
}
