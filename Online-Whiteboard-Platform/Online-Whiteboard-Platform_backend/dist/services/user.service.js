"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const bcrypt = require("bcrypt");
const user_entity_1 = require("../entities/user.entity");
const transform_service_1 = require("./transform.service");
let UserService = class UserService {
    constructor(userRepository, transformer) {
        this.userRepository = userRepository;
        this.transformer = transformer;
    }
    async registerUser(user) {
        if (await this.userRepository.findOne({ username: user.username })) {
            throw new common_1.NotFoundException(`You have to chose another username as this one is already taken!`);
        }
        if (await this.userRepository.findOne({ displayName: user.displayName })) {
            throw new common_1.NotFoundException(`You have to chose another display name as this one is already taken!`);
        }
        const passwordHash = await bcrypt.hash(user.password, 10);
        user.password = passwordHash;
        const registeredUser = await this.userRepository.save(user);
        return this.transformer.toUserResponseDTO(registeredUser);
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.User)),
    __metadata("design:paramtypes", [typeof (_a = typeof typeorm_2.Repository !== "undefined" && typeorm_2.Repository) === "function" ? _a : Object, transform_service_1.TransformService])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map