import { Controller, Get } from "@nestjs/common";

@Controller("")
export class AppController {
  @Get()
  async test(): Promise<{ message: string }> {
    return {
      message: "Only authenticated users can see this.",
    };
  }
}
