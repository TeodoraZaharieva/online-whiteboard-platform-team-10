import { Body, Controller, Post, ValidationPipe, Delete, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { AuthService } from "./auth.service";
import { UserLoginDTO } from "../dtos/login-user.dto";
import { UserCreateDTO } from "../dtos/user-create.dto";
import { UserDTO } from "../dtos/user.dto";

@Controller('api')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @Post('registration')
    async registerUser(
        @Body(new ValidationPipe({ whitelist: true }))
        user: UserCreateDTO): Promise<UserDTO> {

        return await this.authService.register(user);
    }

    @Post('session')
    async login(
        @Body(new ValidationPipe({ whitelist: true }))
        user: UserLoginDTO): Promise<{ token: string }> {

        return await this.authService.login(user);
    }

    @Delete('session')
    @UseGuards(AuthGuard('jwt'))
    async logout(): Promise<{ message: string }> {

        return { message: 'Successful logout!' };
    }
}
