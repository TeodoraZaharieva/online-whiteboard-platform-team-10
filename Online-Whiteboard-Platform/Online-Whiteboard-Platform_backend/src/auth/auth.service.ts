import { Injectable, UnauthorizedException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { JwtService } from "@nestjs/jwt";
import * as bcrypt from "bcrypt";
import { Repository } from "typeorm";
import { User } from "../entities/user.entity";
import { UserLoginDTO } from "../dtos/login-user.dto";
import { JwtPayload } from "../common/jwt-payload";
import { UserCreateDTO } from "../dtos/user-create.dto";
import { UserDTO } from "../dtos/user.dto";
import { UserService } from "../services/user.service";


@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) { }

  async register(user: UserCreateDTO): Promise<UserDTO> {
    return this.userService.registerUser(user);
  }

  async findUserByName(username: string): Promise<User> {
    return await this.userRepository.findOne({
      username,
      isDeleted: false,
    });
  }

  async validateUser(username: string, password: string): Promise<User> {
    const user = await this.findUserByName(username);

    if (!user) {
      return null;
    }

    const isUserValidated = await bcrypt.compare(password, user.password);

    return isUserValidated ? user : null;
  }

  async login(loginUser: UserLoginDTO): Promise<{ token: string }> {
    const user = await this.validateUser(
      loginUser.username,
      loginUser.password
    );

    if (!user) {
      throw new UnauthorizedException("Wrong credentials!");
    }

    const payload: JwtPayload = {
      id: user.id,
      username: user.username,
      displayName: user.displayName,
    };

    const token = await this.jwtService.signAsync(payload);

    return {
      token,
    };
  }
}
