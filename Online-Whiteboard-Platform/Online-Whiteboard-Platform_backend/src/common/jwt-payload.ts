export class JwtPayload {
  id: number;
  username: string;
  displayName: string;
}
