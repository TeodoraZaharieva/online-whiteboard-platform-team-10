import { Controller, Post, UseGuards, Body, Get, Param } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { CanvasCreateDTO } from "../dtos/canvas/canvas.create.dto";
import { CanvasDTO } from "../dtos/canvas/canvas.dto";
import { CanvasService } from "../services/canvas.service";

@Controller("api/:userId/canvases")
export class CanvasController {
  constructor(private readonly canvasService: CanvasService) { }

  @Post()
  @UseGuards(AuthGuard("jwt"))
  async createCanvas(
    @Body() canvasCreateDto: CanvasCreateDTO,
    @Param() userId: any
  ): Promise<Partial<CanvasDTO>> {
    return await this.canvasService.createCanvas(
      canvasCreateDto,
      userId.userId
    );
  }

  @Get()
  async getUserCanvases(
    @Param("userId") userId: string
  ): Promise<Partial<CanvasDTO>[]> {
    return await this.canvasService.getUserCanvases(+userId);
  }

}
