import { Module } from "@nestjs/common";
import { ServicesModule } from "../services/service.module";
import { AuthController } from "../auth/auth.controller";
import { LinesController } from "./lines.controller";
import { CanvasController } from "./canvas.controller";

@Module({
  imports: [ServicesModule],
  controllers: [AuthController, CanvasController, LinesController],
})
export class ControllersModule { }
