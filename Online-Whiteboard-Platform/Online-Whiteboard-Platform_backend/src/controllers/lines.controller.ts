import { Controller, Post, UseGuards, Param, Req, Body } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { LineCreateDTO } from "../dtos/line/line.create.dto";
import { LineDTO } from "../dtos/line/line.dto";
import { LineService } from "../services/line.service";

@Controller("api/canvases/:canvasId/lines")
export class LinesController {
  constructor(private readonly lineService: LineService) { }

  @Post()
  @UseGuards(AuthGuard("jwt"))
  async createLine(
    @Param("canvasId") canvasId: number,
    @Body() lineCreateDto: LineCreateDTO,
    @Req() request: any
  ): Promise<LineDTO> {
    return await this.lineService.createLine(
      +canvasId,
      lineCreateDto,
      +request.user.id
    );
  }
}
