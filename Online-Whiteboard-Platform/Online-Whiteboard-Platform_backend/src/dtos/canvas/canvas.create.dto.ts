import { IsString, IsBoolean } from "class-validator";
import { LineCreateDTO } from "../line/line.create.dto";

export class CanvasCreateDTO {
  @IsString()
  name: string;

  @IsBoolean()
  isPublic: boolean;
  lines: LineCreateDTO[];
}
