import { LineDTO } from "../line/line.dto";
import { UserDTO } from "../user.dto";

export class CanvasDTO {
  id: number;
  name: string;
  isPublic: boolean;
  createDate: Date;
  author: UserDTO;
  lines: LineDTO[];
}
