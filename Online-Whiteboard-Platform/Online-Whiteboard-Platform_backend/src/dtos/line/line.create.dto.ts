import { IsPositive, IsString } from "class-validator";
import { PointDTO } from "./point.dto";

export class LineCreateDTO {

  points: PointDTO[];

  @IsString()
  stroke: string;

  @IsPositive()
  strokeWidth: number;
}
