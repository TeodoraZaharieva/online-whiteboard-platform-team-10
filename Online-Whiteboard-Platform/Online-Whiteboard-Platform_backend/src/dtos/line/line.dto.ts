export class LineDTO {
  id: number;
  points: number[];
  stroke: string;
  strokeWidth: number;
}
