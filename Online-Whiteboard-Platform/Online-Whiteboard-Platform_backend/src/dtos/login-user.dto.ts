import { IsString, Length } from "class-validator";

export class UserLoginDTO {
  @IsString()
  @Length(5, 20, {
    message: "Invalid user credentials!",
  })
  password: string;

  @IsString()
  @Length(2, 20, {
    message: "Invalid user credentials!",
  })
  username: string;
}
