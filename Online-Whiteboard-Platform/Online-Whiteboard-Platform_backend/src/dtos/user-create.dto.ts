import { IsString, Length } from "class-validator";

export class UserCreateDTO {
  @IsString()
  @Length(2, 20, {
    message: "Name should be between 2 and 20 characters!",
  })
  username: string;

  @IsString()
  @Length(5, 20, {
    message: "Password should be between 5 and 20 characters!",
  })
  password: string;

  @IsString()
  @Length(2, 20, {
    message: "Display name should be between 2 and 20 characters!",
  })
  displayName: string;
}
