import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
} from "typeorm";
import { Line } from "./line.entity";
import { User } from "./user.entity";

@Entity("canvases")
export class Canvas {
  @PrimaryGeneratedColumn("increment")
  id: number;

  @Column({
    type: "boolean",
    default: false,
  })
  isDeleted: boolean;

  @Column({
    type: "boolean",
    default: true,
  })
  isPublic: boolean;
  
  @CreateDateColumn()
  createDate: Date;

  @Column({
    type: "nvarchar",
    default: "string",
  })
  name: string;

  @ManyToOne(
    (type) => User,
    (user) => user.canvases,
  )
  author: User;

  @OneToMany(
    (type) => Line,
    (line) => line.canvas,
    { eager: true }
  )
  lines: Line[];
}
