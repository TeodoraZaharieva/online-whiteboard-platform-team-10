import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from "typeorm";
import { Canvas } from "./canvas.entity";
import { Point } from "./point.entity";

@Entity("lines")
export class Line {
  @PrimaryGeneratedColumn("increment")
  id: number;

  @Column({
    type: "boolean",
    default: false,
  })
  isDeleted: boolean;

  @Column()
  stroke: string;

  @Column()
  strokeWidth: number;

  @ManyToOne(
    () => Canvas,
    (canvas) => canvas.lines,
  )
  canvas: Canvas;

  @OneToMany(
    () => Point,
    (point) => point.line,
    { eager: true }
  )
  points: Point[];
}