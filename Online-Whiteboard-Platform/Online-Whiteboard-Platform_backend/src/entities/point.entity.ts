import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Line } from "./line.entity";

@Entity("points")
export class Point {
  @PrimaryGeneratedColumn("increment")
  id: number;

  @Column("bigint")
  posX: number;

  @Column("bigint")
  posY: number;

  @ManyToOne(
    (type) => Line,
    (line) => line.points,
  )
  line: Line;
}
