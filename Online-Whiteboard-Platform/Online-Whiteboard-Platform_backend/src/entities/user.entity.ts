import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, OneToMany } from "typeorm";
import { Canvas } from "./canvas.entity";

@Entity("users")
export class User {
  @PrimaryGeneratedColumn("increment")
  id: number;

  @Column({
    type: "nvarchar",
    nullable: false,
    unique: true,
    length: 20,
  })
  username: string;

  @Column({
    type: "nvarchar",
    nullable: false,
  })
  password: string;

  @Column({
    type: "nvarchar",
    nullable: false,
    default: false,
    unique: true,
    length: 20,
  })
  displayName: string;

  @Column({
    type: "boolean",
    default: false,
  })
  isDeleted: boolean;

  @Column({
    type: "boolean",
    default: false,
  })
  isBanned: boolean;

  @CreateDateColumn()
  createDate: Date;

  @OneToMany(
    (type) => Canvas,
    canvas => canvas.author
  )
  canvases: Canvas[];

}
