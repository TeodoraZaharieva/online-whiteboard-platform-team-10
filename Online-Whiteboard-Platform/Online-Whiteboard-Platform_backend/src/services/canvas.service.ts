import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CanvasCreateDTO } from "../dtos/canvas/canvas.create.dto";
import { CanvasDTO } from "../dtos/canvas/canvas.dto";
import { Canvas } from "../entities/canvas.entity";
import { User } from "../entities/user.entity";
import { Line } from "../entities/line.entity";
import { Point } from "../entities/point.entity";
import { TransformService } from "./transform.service";

@Injectable()
export class CanvasService {
  constructor(
    @InjectRepository(Point)
    private readonly pointRepository: Repository<Point>,
    @InjectRepository(Line)
    private readonly lineRepository: Repository<Line>,
    @InjectRepository(Canvas)
    private readonly canvasRepository: Repository<Canvas>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly transformService: TransformService,
  ) {}

  async createCanvas(
    { name, lines, isPublic }: CanvasCreateDTO,
    userId: number
  ): Promise<Partial<CanvasDTO>> {
    const userFound = await this.userRepository.findOne({
      where: { id: userId, isDeleted: false },
      relations: ["canvases"],
    });

    if (!userFound) {
      throw new NotFoundException(`User not found!`);
    }

    const createdLines: Line[] = [];

    for (const line of lines) {
      const pointsToCreate = line.points.reduce((acc, pos, index) => {
        if (index % 2 === 0) {
          acc.push({ posX: pos, posY: line.points[index + 1] });
        }

        return acc;
      }, []);

      const createdPoints: Point[] = [];

      for (const point of pointsToCreate) {
        const pointToCreate = this.pointRepository.create();
        pointToCreate.line = null;
        pointToCreate.posX = point.posX;
        pointToCreate.posY = point.posY;

        const createdPoint = await this.pointRepository.save(pointToCreate);
        createdPoints.push(createdPoint);
      }

      const lineToCreate = this.lineRepository.create();
      lineToCreate.stroke = line.stroke;
      lineToCreate.strokeWidth = line.strokeWidth || 1;
      lineToCreate.points = createdPoints;

      const createdLine = await this.lineRepository.save(lineToCreate);

      createdLines.push(createdLine);
    }

    const canvasToCreate = this.canvasRepository.create();
    canvasToCreate.name = name;
    canvasToCreate.isPublic = isPublic;
    canvasToCreate.author = userFound;
    canvasToCreate.lines = createdLines;

    const createdCanvas = await this.canvasRepository.save(canvasToCreate);

    return this.transformService.toCanvasResponseDTO(createdCanvas);
  }

  async getUserCanvases(userId: number): Promise<Partial<CanvasDTO>[]> {
    const canvasesFound = await this.canvasRepository.find({
      where: { author: { id: userId }, isPublic: true, isDeleted: false },
    });

    return canvasesFound.map(this.transformService.toCanvasResponseDTO);
  }
}
