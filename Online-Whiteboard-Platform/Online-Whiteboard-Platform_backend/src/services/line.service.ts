import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { LineCreateDTO } from "../dtos/line/line.create.dto";
import { LineDTO } from "../dtos/line/line.dto";
import { Canvas } from "../entities/canvas.entity";
import { Line } from "../entities/line.entity";
import { User } from "../entities/user.entity";
import { Repository } from "typeorm";
import { TransformService } from "./transform.service";

@Injectable()
export class LineService {
  constructor(
    @InjectRepository(Canvas)
    private readonly canvasRepository: Repository<Canvas>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Line) private readonly lineRepository: Repository<Line>,
    private readonly transformService: TransformService
  ) {}

  async createLine(
    canvasId: number,
    lineCreateDto: LineCreateDTO,
    userId: number
  ): Promise<LineDTO> {   

    return null;
  }
}
