import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";
import { JwtStrategy } from "./strategy/jwt-strategy";
import { jwtConstants } from "../constant/secret";
import { User } from "../entities/user.entity";
import { Canvas } from "../entities/canvas.entity";
import { Line } from "../entities/line.entity";
import { Point } from "../entities/point.entity";
import { AuthService } from "../auth/auth.service";
import { UserService } from "./user.service";
import { CanvasService } from "./canvas.service";
import { LineService } from "./line.service";
import { TransformService } from "./transform.service";

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Canvas, Line, Point]),
    PassportModule.register({ defaultStrategy: "jwt" }),
    JwtModule.registerAsync({
      useFactory: async () => ({
        secret: jwtConstants.secret,
        signOptions: {
          expiresIn: jwtConstants.expiresIn,
        },
      }),
    }),
  ],
  providers: [
    TransformService,
    UserService,
    CanvasService,
    LineService,
    AuthService,
    JwtStrategy,
  ],
  exports: [
    TransformService,
    UserService,
    CanvasService,
    LineService,
    AuthService,
    PassportModule,
  ],
})
export class ServicesModule {}
