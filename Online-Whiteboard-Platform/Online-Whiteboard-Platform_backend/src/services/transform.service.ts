import { Injectable } from "@nestjs/common";
import { UserDTO } from "./../dtos/user.dto";
import { User } from "../entities/user.entity";
import { Canvas } from "../entities/canvas.entity";
import { CanvasDTO } from "../../src/dtos/canvas/canvas.dto";

@Injectable()
export class TransformService {
  public toUserResponseDTO(user: User): UserDTO {
    return user
      ? {
          id: user.id,
          username: user.username,
          displayName: user.displayName,
        }
      : null;
  }

  public toCanvasResponseDTO({
    id,
    name,
    isPublic,
    lines,
    createDate,
  }: Canvas): Partial<CanvasDTO> {
    return {
      id,
      name,
      isPublic,
      createDate,
      lines: lines.map((line) => ({
        ...line,
        points: line.points.reduce(
          (acc, { posX, posY }) => acc.concat(+posX, +posY),
          []
        ),
      })),
    };
  }
}
