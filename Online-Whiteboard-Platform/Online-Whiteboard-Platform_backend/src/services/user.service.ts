import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import * as bcrypt from 'bcrypt';
import { UserDTO } from "../dtos/user.dto";
import { UserCreateDTO } from "../dtos/user-create.dto";
import { User } from "../entities/user.entity";
import { TransformService } from "./transform.service";

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly transformer: TransformService
  ) {}

  async registerUser(user: UserCreateDTO): Promise<UserDTO> {
    if (await this.userRepository.findOne({ username: user.username })) {
      throw new NotFoundException(
        `You have to chose another username as this one is already taken!`
      );
    }

    if (await this.userRepository.findOne({ displayName: user.displayName })) {
      throw new NotFoundException(
        `You have to chose another display name as this one is already taken!`
      );
    }

    const passwordHash = await bcrypt.hash(user.password, 10);
    user.password = passwordHash;
    
    const registeredUser = await this.userRepository.save(user);

    return this.transformer.toUserResponseDTO(registeredUser);
  }
}
