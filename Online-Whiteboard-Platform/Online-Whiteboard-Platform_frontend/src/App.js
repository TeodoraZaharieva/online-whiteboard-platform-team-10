import React, { useState, useContext } from "react";
import { BrowserRouter, Switch, Route, NavLink } from "react-router-dom";
import { Container, Row, Col, Navbar, Nav } from "react-bootstrap";
import { HOMEPAGE, LOGIN, REGISTER, APPTOOLS, CREATECANVAS, PROFILE } from "./constants/paths";
import { anonymousGuard, authGuard } from "./auth/guards";
import NotFoundPage from "./components/NotFoundPage";
import HomePage from "./components/HomePage";
import LoginPage from "./components/LoginPage";
import RegisterPage from "./components/RegisterPage";
import AuthContext from "./providers/AuthContext";
import auth from "./auth/auth";
import Alert from "./components/common/Alert";
import AppTools from "./components/AppTools";
import ProfilePage from "./components/ProfilePage";
import CreateCanvas from "./components/CreateCanvas";
import AppContext from "./providers/AppContext";

export const routes = [
  {
    path: HOMEPAGE,
    exact: true,
    component: HomePage,
  },
  {
    path: LOGIN,
    component: anonymousGuard(LoginPage),
  },
  {
    path: REGISTER,
    component: anonymousGuard(RegisterPage),
  },
  {
    path: APPTOOLS,
    component: authGuard(AppTools),
  },
  {
    path: CREATECANVAS,
    component: authGuard(CreateCanvas),
  },
  {
    path: PROFILE,
    component: authGuard(ProfilePage),
  },
  {
    path: "*",
    component: NotFoundPage,
  },
];

const App = () => {
  const appContext = useContext(AppContext);

  const currentCanvasName = appContext.canvasName;
  const [alertProps, setAlertProps] = useState({
    message: "",
    show: false,
    onClose: () => {},
  });

  const showAlert = (message) => {
    setAlertProps({
      message,
      show: true,
      onClose: () =>
        setAlertProps({ message: "", show: false, onClose: () => {} }),
    });
  };

  const [currentUser, setCurrentUser] = useState(auth.getCurrentUser());
  const login = auth.login(setCurrentUser, showAlert);
  const logout = auth.logout(setCurrentUser);

  return (
    <BrowserRouter>
      <AuthContext.Provider
        value={{ currentUser, login, showAlert, appContext }}
      >
        <Row className="m-0 p-0">
          <Col xs="5" sm="12" md="12" lg="12" xl="12" className="m-0 p-0">
            <Navbar
              bg="info"
              variant="dark"
              expand="lg"
              className="px-5"
              style={{ fontSize: "16px" }}
            >
              <Navbar.Brand
                as={NavLink}
                to={HOMEPAGE}
                style={{ fontSize: "16px" }}
              >
                WeeGEN
              </Navbar.Brand>

              <Navbar.Toggle aria-controls="basic-navbar-nav" />

              <Navbar.Collapse>
                <Nav className="ml-auto">
                  <Nav.Link as={NavLink} to={CREATECANVAS}>
                    NEW CANVAS
                  </Nav.Link>
                </Nav>

                <Nav className="ml-auto">
                  <Nav.Link className="text-white" as={NavLink} to={PROFILE}>
                    MY CANVASES
                  </Nav.Link>
                </Nav>
                <Nav className="ml-auto">
                  {currentUser ? (
                    <>
                      <Nav.Link className="text-white">
                        {currentUser.displayName}
                      </Nav.Link>
                      <Nav.Link onClick={logout}>Logout</Nav.Link>
                    </>
                  ) : (
                    <>
                      <Nav.Link as={NavLink} to={LOGIN}>
                        Login
                      </Nav.Link>
                      <Nav.Link as={NavLink} to={REGISTER}>
                        Register
                      </Nav.Link>
                    </>
                  )}
                </Nav>
              </Navbar.Collapse>
            </Navbar>

            <Alert {...alertProps} />

            <Container className="my-5">
              <Switch>
                {routes.map((route) => (
                  <Route key={route.path} {...route} />
                ))}
              </Switch>
            </Container>

            <footer
              style={{
                position: "fixed",
                bottom: 0,
                backgroundColor: "#17a2b8",
                left: 0,
                right: 0,
                color: "#eee",
                padding: "0.5rem",
                textAlign: "center",
              }}
            >
              Online Whiteboard T#L &copy; 2020
            </footer>
          </Col>
        </Row>
      </AuthContext.Provider>
    </BrowserRouter>
  );
};

export default App;

