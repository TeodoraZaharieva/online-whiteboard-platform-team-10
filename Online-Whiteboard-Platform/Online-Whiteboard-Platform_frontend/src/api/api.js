import { REGISTRATION, BASE_URL } from "../constants/urls";
import { createFetchRequest } from "./utils";

const registerUser = (username, displayName, password) => {
  return createFetchRequest(
    REGISTRATION,
    "POST",
    JSON.stringify({ username, displayName, password })
  );
};

const createLine = () => {
  return Promise.resolve();
};

const getUserCanvases = (userId) => {
  return createFetchRequest(
    `${BASE_URL}/${userId}/canvases`, 
    "GET",
    );
};

const createCanvas = (userId, canvas) => {
  return createFetchRequest(
    `${BASE_URL}/${userId}/canvases`,
    "POST",
    JSON.stringify(canvas)
  );
};

export default {
  registerUser,
  createLine,
  createCanvas,
  getUserCanvases,
};
