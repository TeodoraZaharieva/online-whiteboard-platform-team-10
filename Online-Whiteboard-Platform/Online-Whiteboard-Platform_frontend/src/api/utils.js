export const createFetchRequest = async (url, method, body) => {
  return fetch(url, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    method,
    body,
  }).then((response) => response.json());
};
