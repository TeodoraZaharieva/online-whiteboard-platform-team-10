import jwtDecode from "jwt-decode";
import { createFetchRequest } from "../api/utils";
import { SESSION } from "../constants/urls";

const getToken = () => {
  return localStorage.getItem("token") || "";
};

const removeToken = () => {
  return localStorage.removeItem("token");
};

const setToken = ({ token }) => {
  localStorage.setItem("token", token);

  return getCurrentUser();
};

const getCurrentUser = () => {
  try {
    return jwtDecode(getToken());
  } catch {
    return null;
  }
};

const checkForHttpError = (handleError) => (response) => {
  if (response.error) {
    handleError(
      Array.isArray(response.message)
        ? response.message.join(", ")
        : response.message
    );
  }

  return response;
};

const login = (setCurrentUser, handleError) => (userCredentials) => {
  createFetchRequest(SESSION, "POST", JSON.stringify(userCredentials))
    .then(checkForHttpError(handleError))
    .then(setToken)
    .then(setCurrentUser)
    .catch(handleError);
};

const logout = (setCurrentUser) => () => {
  removeToken();
  setCurrentUser(getCurrentUser());
};

const getCurrentUserId = () => {
  return getCurrentUser().id;
};

export default {
  login,
  logout,
  getCurrentUser,
  getCurrentUserId,
};
