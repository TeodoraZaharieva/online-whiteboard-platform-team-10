import React, { useContext } from "react";
import { Redirect } from "react-router-dom";
import AuthContext from "../providers/AuthContext";
import { HOMEPAGE, LOGIN } from "../constants/paths";

export const authGuard = (Component) => (props) => {
  const canActivate = Boolean(useContext(AuthContext).currentUser);

  return canActivate ? <Component {...props} /> : <Redirect to={LOGIN} />;
};

export const anonymousGuard = (Component) => (props) => {
  const canActivate = Boolean(useContext(AuthContext).currentUser);

  return canActivate ? <Redirect to={HOMEPAGE} /> : <Component {...props} />;
};
