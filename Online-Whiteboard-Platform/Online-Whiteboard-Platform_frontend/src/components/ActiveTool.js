export const ActiveTool = {
  NONE: "none",
  DRAW: "draw",
  COLOR: "color",
  ERASER: "eraser",
};
