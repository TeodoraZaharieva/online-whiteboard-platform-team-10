import React, { useState, useContext } from "react";
import { Button } from "react-bootstrap";
import Canvas from "./Canvas";
import { ActiveTool } from "./ActiveTool";
import ColorPalette from "./ColorPalette";
import Eraser from "./Eraser";
import auth from "../auth/auth";
import api from "../api/api";
import AppContext from "../providers/AppContext";
import "../App.css";

const AppTools = () => {
  const appContext = useContext(AppContext);
  const userId = auth.getCurrentUserId();
  const [openPalette, setOpenPalette] = useState(false);
  const [color, setColor] = useState("#000000");
  const [lines, setLines] = useState(appContext.currentCanvas.lines);
  const [currentTool, setCurrentTool] = useState(ActiveTool.NONE);
  const [strokeWidth, setStrokeWidth] = useState(5);
  const [isDraggable, setIsDraggable] = useState(false);
  const [canvas, setCanvas] = useState({});

  const createCanvas = () => {
    api
      .createCanvas(userId, {
        name: appContext.canvasName,
        lines,
        isPublic: appContext.isPublic,
      })
      .then((res) => {
        setCanvas({ ...canvas, isSaved: true });
      });
  };

  const renderMsg = () => {
    return canvas.isSaved ? (
      <div>
        <p className="w-100 my-2" variant="outline-success">
          The canvas is saved!
        </p>
      </div>
    ) : (
      <div className="w-100 my-2" variant="outline-success"></div>
    );
  };

  const switchTool = (currentTool) => {
    setCurrentTool(currentTool);
  };

  const handleOpenPalette = () => {
    setOpenPalette(true);
  };

  return (
    <div className="AppTool">
      <h2>{appContext.canvasName}</h2>

      <Button
        variant="info"
        onClick={() => {
          switchTool(ActiveTool.DRAW);
          setIsDraggable(false);
          if (color === "#ffffff") {
            setColor("#000000");
          }
        }}
        className={currentTool === ActiveTool.DRAW ? "active" : ""}
      >
        DRAW
      </Button>
      <span />

      <Eraser setStrokeWidth={setStrokeWidth} setColor={setColor} />
      <span />

      <ColorPalette
        handleOpenPalette={handleOpenPalette}
        color={color}
        setColor={setColor}
      />
      <span />

      <Button
        variant="info"
        onClick={() => {
          switchTool(ActiveTool.NONE);
          setIsDraggable(true);
        }}
        className={currentTool === ActiveTool.NONE ? "active" : ""}
      >
        DRAG
      </Button>

      <Button variant="info" onClick={createCanvas}>
        SAVE
      </Button>
      <div className="text-center" style={{ color: "pink" }}>
        {renderMsg()}
      </div>
      <span />

      <Canvas
        currentTool={currentTool}
        lines={lines}
        onStopDraw={(line) => {
          setLines((lines) => [...lines, line]);
        }}
        color={color}
        isDraggable={isDraggable}
      />
    </div>
  );
};

export default AppTools;
