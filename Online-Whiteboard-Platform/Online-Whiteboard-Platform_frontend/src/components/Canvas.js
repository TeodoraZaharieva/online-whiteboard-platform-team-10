import React, { useState } from "react";
import { Stage, Layer, Line } from "react-konva";
import { ActiveTool } from "./ActiveTool";

const Canvas = ({ onStopDraw, lines, currentTool, color, isDraggable }) => {
  const [line, setLine] = useState(null);

  const beginDraw = (e) => {
    if (currentTool === ActiveTool.DRAW) {
      setLine([e.evt.layerX, e.evt.layerY]);
    }
  };

  const nextPoint = (e) => {
    if (line) {
      setLine([...line, e.evt.layerX, e.evt.layerY]);
    }
  };

  const stopDraw = (e) => {
    if (line) {
      onStopDraw({ stroke: color, points: line });
      setLine(null);
    }
  };

  return (
    <Stage
      width={1200}
      height={800}
      onMouseDown={beginDraw}
      onMouseMove={nextPoint}
      onMouseUp={stopDraw}
    >
      <Layer>
        {lines.map((props, index) => (
          <Line key={index} {...props} draggable={isDraggable} />
        ))}
      </Layer>

      <Layer>
        {line ? <Line x={0} y={0} points={line} stroke={color} /> : null}
      </Layer>
    </Stage>
  );
};

export default Canvas;
