import React, { useState } from "react";
import { CompactPicker } from "react-color";
import "react-bootstrap";

const ColorPalette = ({ color, setColor }) => {
  const [displayColorPicker, setDisplayColorPicker] = useState(false);

  const handleClick = () => {
    setDisplayColorPicker(!displayColorPicker);
  };

  const handleClose = () => {
    setDisplayColorPicker(false);
  };

  const popover = {
    position: "absolute",
    zIndex: "2",
  };

  const cover = {
    position: "fixed",
    top: "0px",
    right: "0px",
    bottom: "0px",
    left: "0px",
    padding: "",
  };
  return (
    <div className="btn btn-info" onClick={handleClick}>
      <div>COLOR</div>
      {displayColorPicker ? (
        <div style={popover}>
          <div style={cover} onClick={handleClose} />
          <CompactPicker
            color={color}
            onChange={(color) => setColor(color.hex)}
          />
        </div>
      ) : null}
    </div>
  );
};

export default ColorPalette;
