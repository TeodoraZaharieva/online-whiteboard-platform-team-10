import React, { useContext, useState } from "react";
import { Form, Button, Card } from "react-bootstrap";
import AppContext from "../providers/AppContext";
import auth from "../auth/auth";

const CreateCanvas = ({ history }) => {
  const appContext = useContext(AppContext);
  const userId = auth.getCurrentUserId();
  const defaultCanvasFormState = { canvasName: "", isPublic: true };
  const [canvasForm, setCanvasForm] = useState(defaultCanvasFormState);

  const onInputChange = (field) => ({ target }) => {
    setCanvasForm({ ...canvasForm, [field]: target.value });
  };

  const addCanvasName = () => {
    appContext.canvasName = canvasForm.canvasName;
    appContext.isPublic = canvasForm.isPublic;
    appContext.currentCanvas = {
      id: 0,
      lines: [],
    };
    history.push(`/apptools`);
  };

  return (
    <div>
      <Card className="mx-auto" style={{ width: "360px", marginTop: "10rem" }}>
        <Card.Body>
          <Form>
            <Form.Group>
              <Form.Label>New Canvas</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Canvas Name"
                onChange={onInputChange("canvasName")}
                value={canvasForm.canvasName}
              />
            </Form.Group>

            <Button
              variant="info"
              type="submit"
              className="w-100 mt-3"
              style={{ fontSize: "1em" }}
              onClick={(e) => {
                e.preventDefault();
                setCanvasForm((prev) => ({
                  ...prev,
                  isPublic: !prev.isPublic,
                }));
              }}
            >
              {canvasForm.isPublic ? "Public" : "Private"}
            </Button>
            <Button
              onClick={(e) => {
                e.preventDefault();
                addCanvasName();
              }}
              disabled={!canvasForm.canvasName}
              variant="info"
              type="submit"
              className="w-100 mt-3"
              style={{ fontSize: "1em" }}
            >
              Save
            </Button>
            <Button
              onClick={(e) => {
                e.preventDefault();
                history.goBack();
              }}
              variant="info"
              type="submit"
              className="w-100 mt-3"
              style={{ fontSize: "1em" }}
            >
              Cancel
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </div>
  );
};

export default CreateCanvas;
