import React from "react";
import { Button } from "react-bootstrap";

const Eraser = ({ setColor }) => {
  const handleEraser = (event) => {
    event.preventDefault();
    setColor("#ffffff");
  };

  return (
    <Button variant="info" onClick={handleEraser}>
      ERASER
    </Button>
  );
};

export default Eraser;
