import React from "react";

const img1 = require("./common/Images/HP_01.png");
const img2 = require("./common/Images/HP_02.png");

const HomePage = () => {
  return (
    <div className="container">
      <div>
        <img src={img1} width="1000px" />
        <img src={img2} width="1000px" />
      </div>
    </div>
  );
};

export default HomePage;
