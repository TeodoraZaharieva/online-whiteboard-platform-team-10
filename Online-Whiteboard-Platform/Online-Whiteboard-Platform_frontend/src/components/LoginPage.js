import React, { useState, useContext } from "react";
import { Form, Button, Card } from "react-bootstrap";
import AuthContext from "../providers/AuthContext";

const defaultLoginFormState = { username: "", password: "" };

const LoginPage = () => {
  const [loginForm, setLoginForm] = useState(defaultLoginFormState);
  const auth = useContext(AuthContext);

  const onInputChange = (field) => ({ target }) => {
    setLoginForm({ ...loginForm, [field]: target.value });
  };

  const login = (event) => {
    event.preventDefault();
    auth.login(loginForm);
    setLoginForm(defaultLoginFormState);
  };

  return (
    <Card className="mx-auto" style={{ width: "360px", marginTop: "10rem" }}>
      <Card.Body>
        <Form onSubmit={login}>
          <Form.Group>
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter username"
              onChange={onInputChange("username")}
              value={loginForm.username}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={onInputChange("password")}
              value={loginForm.password}
            />
          </Form.Group>

          <Button
            disabled={!loginForm.username || !loginForm.password}
            variant="info"
            type="submit"
            className="w-100 mt-3"
            style={{ fontSize: "1em" }}
          >
            Login
          </Button>
        </Form>
      </Card.Body>
    </Card>
  );
};

export default LoginPage;
