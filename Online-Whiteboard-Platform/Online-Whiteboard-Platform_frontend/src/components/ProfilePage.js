import React, { useContext, useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import Canvas from "./Canvas";
import auth from "../auth/auth";
import api from "../api/api";
import AppContext from "../providers/AppContext";
import { APPTOOLS } from "../constants/paths";

const ProfilePage = ({ history }) => {
  const appContext = useContext(AppContext);
  const [canvases, setCanvases] = useState([]);
  const userId = auth.getCurrentUserId();
 
  useEffect(() => {
    api.getUserCanvases(userId).then((canvases) => {
      setCanvases(canvases);
      appContext.canvases.push(canvases);
    });
  }, [userId]);

  const navigateToActiveCanvas = (id, lines, name) => {
    appContext.canvasName = name;
    appContext.currentCanvas = { id, lines, name };
    history.push(APPTOOLS);
  };

  const onBack = () => {
    history.goBack();
  };

  return (
    <div className="AppTool">
      <Button
          style={{ minWidth: "100px" }}
          className="my-4"
          variant="outline-info"
          onClick={onBack}
        >
           BACK
        </Button>
      {canvases.map(({ id, lines, name, isPublic }) => (
        <Card
          key={id}
          style={{ cursor: "pointer" }}
          onClick={() => navigateToActiveCanvas(id, lines, name, isPublic)}
        >
          <Card.Title className="text-right" style={{ color: "black" }}>{`${name} - ${
            isPublic ? "public canvas" : "private canvas"
          }`}</Card.Title>
          <Canvas
            currentTool={null}
            lines={lines || []}
            onStopDraw={() => {}}
            color={null}
          />
        </Card>
      ))}
    </div>
  );
};

export default ProfilePage;

