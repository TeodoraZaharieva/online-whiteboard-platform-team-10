import React, { useState } from "react";
import { Form, Button, Card } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { LOGIN } from "../constants/paths";
import api from "../api/api";

const defaultRegisterFormState = {
  username: "",
  displayName: "",
  password: "",
  confirmPassword: "",
};

const RegisterPage = () => {
  const history = useHistory();
  const [registerForm, setRegisterForm] = useState(defaultRegisterFormState);

  const onRegister = (event) => {
    event.preventDefault();

    const { username, displayName, password } = registerForm;
    setRegisterForm(defaultRegisterFormState);

    api.registerUser(username, displayName, password).then((response) => {
      history.push(LOGIN);
    });
  };

  const onInputChange = (field) => ({ target }) => {
    setRegisterForm({ ...registerForm, [field]: target.value });
  };

  return (
    <Card className="mx-auto" style={{ width: "360px", marginTop: "10rem" }}>
      <Card.Body>
        <Form onSubmit={onRegister}>
          <Form.Group>
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter username"
              onChange={onInputChange("username")}
              value={registerForm.username}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Display name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter display name"
              onChange={onInputChange("displayName")}
              value={registerForm.displayName}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter password"
              onChange={onInputChange("password")}
              value={registerForm.password}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Confirm password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter confirm password"
              onChange={onInputChange("confirmPassword")}
              value={registerForm.confirmPassword}
            />
          </Form.Group>

          <Button
            variant="info"
            type="submit"
            className="w-100 mt-3"
            style={{ fontSize: "1em" }}
          >
            Register
          </Button>
        </Form>
      </Card.Body>
    </Card>
  );
};

export default RegisterPage;
