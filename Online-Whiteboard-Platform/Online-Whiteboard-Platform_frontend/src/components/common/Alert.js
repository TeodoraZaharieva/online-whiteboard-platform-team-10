import React from "react";
import PropTypes from "prop-types";
import { Toast } from "react-bootstrap";

const Alert = ({ message, show, onClose }) => {
  return (
    <Toast
      style={{
        position: "absolute",
        right: "0%",
        top: "10%",
        backgroundColor: "red",
        color: "white",
      }}
      onClose={onClose}
      show={show}
      delay={3000}
      autohide
    >
      <Toast.Body>{message}</Toast.Body>
    </Toast>
  );
};

Alert.propTypes = {
  message: PropTypes.string,
  show: PropTypes.bool,
  onClose: PropTypes.func,
};

export default Alert;
