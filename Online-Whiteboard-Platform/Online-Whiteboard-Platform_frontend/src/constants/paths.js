export const HOMEPAGE = "/";
export const LOGIN = "/login";
export const REGISTER = "/register";
export const APPTOOLS = "/apptools";
export const PROFILE = "/profile";
export const CREATECANVAS = "/new-canvas";

