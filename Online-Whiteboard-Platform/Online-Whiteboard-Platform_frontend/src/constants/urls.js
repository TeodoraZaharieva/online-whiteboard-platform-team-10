export const BASE_URL = "http://localhost:5000/api";
export const REGISTRATION = BASE_URL + "/registration";
export const SESSION = BASE_URL + "/session";
export const LINES = BASE_URL + "/lines";
export const CANVASES = BASE_URL + "/canvases";

