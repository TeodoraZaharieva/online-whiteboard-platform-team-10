import { createContext } from "react";

const AppContext = createContext({
  canvases: [],
  canvasName: "",
  currentCanvas: {
    id: 0,
    lines: [],
    name: "",
    isPublic: true,
  },
  setAppState: () => {},
});

export default AppContext;
